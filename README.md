# instrumenpoche

Le dépôt de code a déménagé et se trouve désormais sur
https://git.sesamath.net/sesamath/instrumenpoche

Vous pouvez indiquez dans vos dépendances (package.json)
```
  "instrumenpoche": "git+https://git.sesamath.net/sesamath/instrumenpoche.git",
```
